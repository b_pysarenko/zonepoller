package com.gemicle.zonepoller.poller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@Component
public class ZonePoller {

    @Autowired
    @Qualifier("url")
    private String urlPattern;

    @Autowired
    @Qualifier("transport")
    private List<String> transportIds;

    @Autowired
    private RestTemplate template;

    @Scheduled(fixedDelayString = "${polling.timeout}")
    public void poll() {
		ExecutorService executor = Executors.newFixedThreadPool(30);
		transportIds.forEach(x->executor.execute(new Taska(x)));
    }

    public void getZones(String transportId) {
        final String url = String.format(urlPattern, transportId);
        try {
            log.info(url);
            String response = template.getForObject(url, String.class);
            log.info("Transport {} : {}", transportId, response);
        } catch (RestClientException e) {
            log.error(e.getMessage());
        }
    }

    class Taska implements  Runnable {

    	private String id;

    	public Taska(String id){
			this.id = id;
		}

		@Override
		public void run() {
			getZones(this.id);
		}

	}

}

package com.gemicle.zonepoller.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;


@Configuration
public class TemplateConfig {

    @Value("${zone.data.url}")
    private String url;

    @Value("${transport.ids}")
    private String transport;


    @Bean(name = "transport")
    public List<String> getTransportList(){
        return Arrays.asList(transport.split(";"));
    }

    @Bean(name = "url")
    public String getUrl(){
        return url;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

}
